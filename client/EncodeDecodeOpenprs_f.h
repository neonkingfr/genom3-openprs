<'
# Copyright (c) 2012-2013 LAAS/CNRS
# All rights reserved.
#
# Redistribution and use  in source  and binary  forms,  with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of  source  code must retain the  above copyright
#      notice and this list of conditions.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice and  this list of  conditions in the  documentation and/or
#      other materials provided with the distribution.
#
#                                      Felix Ingrand on July 20 2012
#

if {[llength $argv] != 1} { error "expected arguments: component" }
lassign $argv component

set types [$component types public]

# compute handy shortcuts
set comp [$component name]
set COMP [string toupper [$component name]]

lang c
'>

<'foreach s [$component services] {'>

/* <"[--- Service [$s name] encoding ---------------------------]"> */


PBoolean encode_g3_<"$comp">_<"[$s name]">_input(
     Expression *tc,
     struct genom_<"$comp">_<"[$s name]">_input *str);

Term *decode_g3_<"$comp">_<"[$s name]">_output(
     struct genom_<"$comp">_<"[$s name]">_output *str);
<'}'>


<'foreach p [$component ports out] {'>

/* <"[--- Port [$p name] decoding ---------------------------]"> */

Term *decode_g3_<"$comp">_<"[$p name]">_port(
     const <"[[$p datatype] argument reference data]">);
<'}'>

Term *genom_oprs_<"$comp">_exception(genom_event e, const void *detail);
