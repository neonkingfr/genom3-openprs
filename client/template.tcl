#
# Copyright (c) 2012-2013 LAAS/CNRS
# All rights reserved.
#
# Redistribution  and  use  in  source  and binary  forms,  with  or  without
# modification, are permitted provided that the following conditions are met:
#
#   1. Redistributions of  source  code must retain the  above copyright
#      notice and this list of conditions.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice and  this list of  conditions in the  documentation and/or
#      other materials provided with the distribution.
#
# THE SOFTWARE  IS PROVIDED "AS IS"  AND THE AUTHOR  DISCLAIMS ALL WARRANTIES
# WITH  REGARD   TO  THIS  SOFTWARE  INCLUDING  ALL   IMPLIED  WARRANTIES  OF
# MERCHANTABILITY AND  FITNESS.  IN NO EVENT  SHALL THE AUTHOR  BE LIABLE FOR
# ANY  SPECIAL, DIRECT,  INDIRECT, OR  CONSEQUENTIAL DAMAGES  OR  ANY DAMAGES
# WHATSOEVER  RESULTING FROM  LOSS OF  USE, DATA  OR PROFITS,  WHETHER  IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR  OTHER TORTIOUS ACTION, ARISING OUT OF OR
# IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
#                                           Anthony Mallet on Mon Mar 12 2012
#

# Pocolibs Openprs client library and OPs generation.

template usage {*}{
    "\n"
    "OpenPRS client library and OP for pocolibs-based components.\n"
    "\n"
    "This template generates an OpenPRS client library for GenoM component built\n"
    "with any (a priori) middleware.\n"
    "\n"
    "Supported options:\n"
    "  -C, --directory=dir\toutput files in dir\n"
    "  -p, --preserve\tdo not overwrite existing files\n"
    "  -h. --help\t\tprint usage summary (this text)"
}

# defaults
set odir client/openprs
engine mode +overwrite +move-if-change

# parse options
template options {
    -C - --directory	{ set odir [template arg] }
    -p - --preserve	{ engine mode -overwrite }
    -h - --help		{ puts [template usage]; exit 0 }
}

# add common services to dotgen specification
dotgen parse file [file join [dotgen template builtindir] common/genom.gen]

# parse input
set input [list]
foreach f $argv {
  dotgen parse file $f
  lappend input [file normalize $f]
}

# only one component
set comp [dotgen components]
if {[llength $comp] > 1} {
  template fatal "component compositing not supported"
}
engine chdir $odir

# require utility procs
template require ../common/typeutil.tcl

set types [$comp types public]

set lisp_header { ;;; <"[--- Generated by [dotgen genom version]. Do not edit - ]">}

# common header for all files (copyright info from .gen file)
set header {/* <"[--- Generated by [dotgen genom version]. Do not edit - ]"> */

<'if {![catch {dotgen input notice} notice]} {
  puts [lang c; comment $notice]
}'>
}

# client source files
set client_files {
  RequestOpenprs.c
  EncodeDecodeOpenprs.c
  EncodeDecodeOpenprs_f.h
}

# client files with a lisp syntax
set lisp_client_files {
  Openprs.opf
  Openprs.sym
  Openprs.inc.input
}

foreach c [dotgen components] {
  foreach f $client_files {
    template parse args $c string $header \
        file $f file [$c name][file tail $f]
  }
  foreach f $lisp_client_files {
    template parse args $c string $lisp_header \
      file $f file [$c name][file tail $f]
  }
}

# template parse args [list $types] \
#     string $header file ../../common/serialize.h file src/serialize.h
# template parse args [list $types] \
#     string $header file ../../common/typecopy.h file src/typecopy.h

# setup build environment
template parse file bootstrap.sh file bootstrap.sh
template parse args [list $input] file client.Makefile.am file Makefile.am
template parse file client.configure.ac file configure.ac
# template parse file \
#     ../../common/autoconf/ax_pthread.m4 file autoconf/ax_pthread.m4
template parse file openprs-client.pc.in file [$comp name]-openprs-client.pc.in
#template link private.h src/private.h

file mkdir $odir/autoconf

set deps [list]
foreach d [concat [dotgen input deps] [template deps]] {
  lappend deps "regen: $d"
  lappend deps "$d:"
}
engine mode +overwrite -move-if-change
template parse raw [join $deps "\n"]\n file regen
