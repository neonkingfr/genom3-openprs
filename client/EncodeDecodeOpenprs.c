<'
# Copyright (c) 2012-2017 LAAS/CNRS
# All rights reserved.
#
# Redistribution and use  in source  and binary  forms,  with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of  source  code must retain the  above copyright
#      notice and this list of conditions.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice and  this list of  conditions in the  documentation and/or
#      other materials provided with the distribution.
#
#                                      Felix Ingrand on July 20 2012
#

if {[llength $argv] != 1} { error "expected arguments: component" }
lassign $argv component

set types [$component types public]

# compute handy shortcuts
set comp [$component name]
set COMP [string toupper [$component name]]

lang c
'>

#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <macro-pub.h>
#include <opaque-pub.h>
#include <constant-pub.h>
#include <oprs-type-pub.h>
#include <oprs-type_f-pub.h>
#include <lisp-list_f-pub.h>

#include <pu-genom_f.h>
#include <pu-mk-term_f.h>
#include <pu-parse-tl_f.h>

#include "<"$comp">/c/client.h"
#include "<"$comp">EncodeDecodeOpenprs_f.h"

/* --- declaration of internal data types----------------------------------------- */

<'foreach t $types {'>
static __inline__ PBoolean  encode_g3_<"[$t mangle]">(
     Term *term,
     <"[$t argument reference]">);
static __inline__ Term *decode_g3_<"[$t mangle]">(
     <"[$t argument value]">);
<'}'>

<'foreach s [$component services] {

#    puts stderr "[oprs_service_va $s]"
#    puts stderr "[oprs_service_va_doc $s]"
'>

/* <"[--- Service [$s name] encoding / decoding ---------------------------]"> */

PBoolean encode_va_g3_<"$comp">_<"[$s name]">_input(
     Expression *tc,
     struct genom_<"$comp">_<"[$s name]">_input *in)
{
     return PUGetOprsVarArgG3Parameters(tc, TRUE, <" [oprs_service_nva $s] ">
					<" [oprs_service_va $s] ">
	  );
}

PBoolean encode_g3_<"$comp">_<"[$s name]">_input(
     Expression *tc,
     struct genom_<"$comp">_<"[$s name]">_input *in)
{
     static Symbol symbol_<"$comp">_<"[$s name]">_input = NULL;
     if (! symbol_<"$comp">_<"[$s name]">_input)
	  symbol_<"$comp">_<"[$s name]">_input = declare_atom("<"$comp">_<"[$s name]">_input");

     if (pu_check_ttc_symbol_alter(tc, symbol_<"$comp">_<"[$s name]">_input)) {
	  int elt = 1;
	  Expression *tc_tmp;

  <'	lappend opredicate $comp\_[$s name]
	lappend opredicate $comp\_[$s name]_report
	lappend opredicate $comp\_[$s name]_async
  	lappend predicate $comp\_[$s name]_output
  	lappend predicate $comp\_[$s name]_input
        foreach p [$s parameters in inout] {
          lappend function [$p name] '>
	  if (! PUGetOprsTermCompSpecArg(tc, elt, EXPRESSION, &tc_tmp))
	       return FALSE;

	  static Symbol symbol_<"[$p name]"> = NULL;
	  if (! symbol_<"[$p name]">)
	       symbol_<"[$p name]"> = declare_atom("<"[$p name]">");

          if (!pu_check_ttc_symbol_strict(tc_tmp, symbol_<"[$p name]">))
               return FALSE;

	  if (! encode_g3_<"[[$p type] mangle]">(EXPR_TERM1(tc_tmp),
						    <"[[$p type] pass reference in->[$p name]]">))
	       return FALSE;
	  elt++;

<'    }'>
          return TRUE;
     } else {

	  static Symbol symbol_VarArg = NULL;
	  if (! symbol_VarArg)
	       symbol_VarArg = declare_atom("VarArg");

	  if (pu_check_ttc_symbol_alter(tc, symbol_VarArg)) 
	       return encode_va_g3_<"$comp">_<"[$s name]">_input(tc, in);
	  else {
	       fprintf(stderr, "encode_g3__<"$comp">_<"[$s name]">_input: your argument should either start with <"$comp">_<"[$s name]">_input or VarArg.\n");
	       
	       return FALSE;
	  }
     }
}

Term *decode_g3_<"$comp">_<"[$s name]">_output(
     struct genom_<"$comp">_<"[$s name]">_output *out)
{
     static Pred_Func_Rec *fr = NULL;
     if (!fr) fr= find_or_create_pred_func(declare_atom("<"$comp">_<"[$s name]">_output"));

     TermList tl = sl_make_slist();
     TermList tl2;
     Term *res;

<'    foreach p [$s parameters inout out] {'>
     tl2 = sl_make_slist();

     static Pred_Func_Rec *fr_<"[$p name]"> = NULL;
     if (! fr_<"[$p name]">) 
	   fr_<"[$p name]"> = find_or_create_pred_func(declare_atom("<"[$p name]">"));

     add_to_tail(tl2,decode_g3_<"[[$p type] mangle]">(<"[[$p type] pass value out->[$p name]]">));
     res = build_term_expr(build_expr_pfr_terms(fr_<"[$p name]">, tl2));
     add_to_tail(tl, res);

<'    }'>
     return build_term_expr(build_expr_pfr_terms(fr, tl));
}

<'}'>

<'foreach p [$component ports out] {
  	lappend predicate $comp\_[$p name]
'>

/* <"[--- Port [$p name] decoding ---------------------------------------]"> */

Term *decode_g3_<"$comp">_<"[$p name]">_port(
       const <"[[$p datatype] argument reference data]">)
{
     static Pred_Func_Rec *fr = NULL;
     if (!fr) fr = find_or_create_pred_func(declare_atom("<"$comp">_<"[$p name]">"));

     TermList tl = sl_make_slist();

<'  set data [[$p datatype] dereference reference data]'>
     add_to_tail(tl,decode_g3_<"[[$p datatype] mangle]">(<"[[$p datatype] pass value $data]">));
     return build_term_expr(build_expr_pfr_terms(fr, tl));
}

<'}'>


<'foreach t $types {

#    puts stderr [oprs_type_glob $t]

'>

/* <"[--- [$t mangle] encoding/decoding---------------------------]"> */

static PBoolean encode_g3_<"[$t mangle]">(
     Term *term,
     <"[$t argument reference data]">)
{
<'
  switch -glob -- [$t kind] {
    typedef - {* member} {'>
    return encode_g3_<"[[$t type] mangle]">(term,data);
<'
    }
    enum {'>
   Symbol val;
  if (! PU_bind_atom(&val,term))
    return FALSE;
<'      foreach e [$t members] {'>
<'     lappend atom [$e name] '>
   if (strcmp(val, "<"[$e name]">")==0) 
     *data = <"[$e cname]">;
   else 
<'      }'>
     {
       fprintf(stderr, "encode_g3_<"[$t mangle]">: invalid enum value %s.\n", val);
       return FALSE;
     }
  return TRUE;
<'
    }
    float {'>
   if (!PU_bind_float(data,term))
     return FALSE;
  return TRUE;
<'
    }
    double {'>
   if (!PU_bind_double(data,term))
     return FALSE;
  return TRUE;
<'
    }
    string {'>
   char *tmp;
  if (!PU_bind_string(&tmp, term))
    return FALSE;
<'      if {[catch {$t length} l]} {'>
   if (*data) free(*data);
  *data=strdup(tmp);
  if (! *data) return FALSE;
<'      } else {'>
   if (strlen(tmp) + 1 > <"$l">) {
     data[0] = '\0';
     fprintf(stderr, "encode_g3_<"[$t mangle]">: string \"%s\" to long to fit in a char[<"$l">].\n", tmp);
   } else
     strncpy(data, tmp, strlen(tmp)+1);
<'      }'>
   return TRUE;
<'
    }
    short {'>
   if (!PU_bind_short(data, term))
     return FALSE;
   return TRUE;
<'
    }
    int {'>
   if (!PU_bind_integer(data, term))
     return FALSE;
   return TRUE;
<'
    }
    boolean {'>
   int data_int;
   if (!PU_bind_integer(&data_int, term))
     return FALSE;
   *data = data_int;
   return TRUE;
<'
    }
    char {'>
   char *tmp;
   if (!PU_bind_string(&tmp, term))
     return FALSE;
   if (strlen(tmp) > 1) 
     fprintf(stderr, "encode_g3_<"[$t mangle]">: string \"%s\" to long to fit in a char.\n", tmp);
   *data = tmp[0];
   return TRUE;
<'
    }
    long {'>
   int data_tmp;
   if (!PU_bind_integer(&data_tmp, term))
     return FALSE;
   *data = data_tmp;
   return TRUE;
<'
    }
    {unsigned *} {'>
   int data_tmp;
   if (!PU_bind_integer(&data_tmp, term))
     return FALSE;
   if (data_tmp < 0) {
     fprintf(stderr, "encode_g3_<"[$t mangle]">: got a negative number to be sored in an unsigned.\n");
     return FALSE;
   }
   *data = data_tmp;
   return TRUE;
<'
    }
    sequence {'>
     size_t i, j;
     L_List l_list;
     if (!PU_bind_l_list(&l_list, term))
	  return FALSE;
     i = l_length(l_list);
<'      if {[catch {$t length} l]} {'>
     if (genom_sequence_reserve(data, i)) return FALSE;
<'      } else {'>
     if (i >= <"$l">) {
        fprintf(stderr, "encode_g3_<"[$t mangle]">: sequence of size %zu longer than expected  <"$l">.\n", i);
	return FALSE;
     }
<'      }'>
     for (j=0; j<i; j++) {
	  Term *term = l_car(l_list);
	  
	  if (! (encode_g3_<"[[$t type] mangle]">(term,
						  <"[[$t type] pass reference data->_buffer\[j\]]">)))
	    return FALSE;
	  l_list = l_cdr(l_list);
     }
     return TRUE;
<' }'>
<'    optional {'>
     Symbol atom;
     if (PU_bind_atom(&atom, term) && (atom == nil_sym)) {
       data->_present = false;
       return TRUE;		/* Optional not present... */
     } else  {
       data->_present = true;
       return encode_g3_<"[[$t type] mangle]">(term, <"[[$t type] pass reference data->_value]">);
     }
<'    }'>
<'    array {'>
     size_t i, j;
     L_List l_list;
     if (!PU_bind_l_list(&l_list, term))
	  return FALSE;
     i = l_length(l_list);
     if (i > <"[$t length]">) {
	  fprintf(stderr, "encode_g3_<"[$t mangle]">: array size %zu bigger than expected  <"[$t length]">.\n", i);
	  return FALSE;
     }
     for (j=0; j<i; j++) {
	  Term *term2 = l_car(l_list);
	  
	  if (!encode_g3_<"[[$t type] mangle]">(term2,
						<"[[$t type] pass reference data\[j\]]">))
	    return FALSE;
	  l_list = l_cdr(l_list);
     }
     return TRUE;
<'    } '>
<'    struct - exception {'>
     int elt = 1;
     Expression *tc_tmp;

     if (! PU_bind_expr(&tc_tmp, term)) return FALSE;

     static Symbol symbol_<"[$t mangle]"> = NULL;
     if (! symbol_<"[$t mangle]">)
	  symbol_<"[$t mangle]"> = declare_atom("<"[$t mangle]">");

     if (!pu_check_ttc_symbol_strict(tc_tmp, symbol_<"[$t mangle]">))
       return FALSE;
  <'	lappend predicate [$t mangle] '>

<'      foreach e [$t members] {'>
     if (! PUGetOprsTermCompSpecArg(term->u.expr, elt, EXPRESSION, &tc_tmp))
       return FALSE;

     static Symbol symbol_<"[$e name]"> = NULL;
     if (! symbol_<"[$e name]">)
	  symbol_<"[$e name]"> = declare_atom("<"[$e name]">");

     if (!pu_check_ttc_symbol_strict(tc_tmp, symbol_<"[$e name]">))
       return FALSE;
 <'     lappend function [$e name] '>
     if (! encode_g3_<"[[$e type] mangle]">(EXPR_TERM1(tc_tmp), <"[$e pass reference data->[$e name]]">))
       return FALSE;
     elt++;

<'        }'>
     return TRUE;
<'    } '>
<'    union {'>
     Expression *tc_tmp;

     if (! PU_bind_expr(&tc_tmp, term)) return FALSE;

     static Symbol symbol_<"[$t mangle]"> = NULL;
     if (! symbol_<"[$t mangle]">)
	  symbol_<"[$t mangle]"> = declare_atom("<"[$t mangle]">");

     if (!pu_check_ttc_symbol_strict(tc_tmp, symbol_<"[$t mangle]">))
       return FALSE;
<'	lappend predicate [$t mangle] '>
     if (! PUGetOprsTermCompSpecArg(term->u.expr, 1, EXPRESSION, &tc_tmp))
       return FALSE;
<'     set e [$t discr] '>
     static Symbol symbol_<"[$e name]"> = NULL;
     if (! symbol_<"[$e name]">)
	  symbol_<"[$e name]"> = declare_atom("<"[$e name]">");

     if (!pu_check_ttc_symbol_strict(tc_tmp, symbol_<"[$e name]">))
       return FALSE;
 <'     lappend function [$e name] '>
     if (! encode_g3_<"[$e mangle]">(EXPR_TERM1(tc_tmp), <"[$e pass reference data->_d]">))
       return FALSE;

     switch (data->_d) {
<'      foreach e [$t members] {'>
<'        foreach v [$e value] {'>
<'          if {$v != ""} {'>
     case <"[language::cname $v]">:
<'          } else {'>
     default:
<'          }'>
<'        }'>
	 {
	   if (! PUGetOprsTermCompSpecArg(term->u.expr, 2, EXPRESSION, &tc_tmp))
	     return FALSE;

	   static Symbol symbol_<"[$e name]"> = NULL;
	   if (! symbol_<"[$e name]">)
	     symbol_<"[$e name]"> = declare_atom("<"[$e name]">");
	   
	   if (!pu_check_ttc_symbol_strict(tc_tmp, symbol_<"[$e name]">))
	     return FALSE;
 <'     lappend function [$e name] '>
           if (! encode_g3_<"[[$e type] mangle]">(EXPR_TERM1(tc_tmp), <"[$e pass reference data->_u.[$e name]]">))
	     return FALSE;
	   break;
	 }
<'        }'>
   }
     return TRUE;
<'    } '>
<'    default {'>
      fprintf(stderr, "encode_g3_<"[$t mangle]"> has not been properly defined... report this to the genom3_openprs template maintainer (felix@laas.fr).\n");
     return FALSE;
<' puts stderr "encode_g3_...: unexpected type: [$t kind]" 
     }
  }
'>
}

static Term *decode_g3_<"[$t mangle]">(
     <"[$t argument value data]">)
{
<'
    switch -glob -- [$t kind] {
    typedef - {* member} {'>
     return decode_g3_<"[[$t type] mangle]">(data);
<'
    }
    enum {'>
<'      foreach e [$t members] {'>
     if (data == <"[$e cname]">)
	  return PUMakeTermAtom("<"[$e name]">");
     else 
<'      }'>
          {
           fprintf(stderr, "decode_g3_<"[$t mangle]">: invalid  value %d for enum.\n", data);
           return PUMakeTermAtom("UNKNOWN_ENUM_VALUE_<"[$t mangle]">");
	   }
<'
    }
    double {'>
     return PUMakeTermDouble(data);
<'
    }
    float {'>
     return PUMakeTermFloat(data);
<'
    }
    string {'>
     return PUMakeTermString(data);
<'
    }
    int - short - long - boolean - {unsigned *} {'>
     return PUMakeTermInteger(data);
<'
    }
    char {'>
     char tmp[2];

     tmp[0] = data; tmp[1] = '\0';
     return PUMakeTermString(tmp);
<'
    }
    sequence {'>
     size_t i;
     TermList tl2 = sl_make_slist();
     Term *term;

     for (i=0; i<data->_length; i++) {
	  term = decode_g3_<"[[$t type] mangle]">(<"[[$t type] pass value {data->_buffer[i]}]">);
	  add_to_tail(tl2, term);
     }
     return build_term_l_list_from_c_list(tl2);
<'
    }'>
<'    optional {'>
     if (data->_present) {
       return  decode_g3_<"[[$t type] mangle]">(<"[[$t type] pass value {data->_value}]">);
     } else {
       return PUMakeTermNil();
     }
<'    }'>
<'   array {'>
     size_t i;
     TermList tl2 = sl_make_slist();
     Term *term;

     for (i=0; i<<"[$t length]">; i++) {
	  term = decode_g3_<"[[$t type] mangle]">(<"[[$t type] pass value {data[i]}]">);
	  add_to_tail(tl2, term);
     }
     return build_term_l_list_from_c_list(tl2);
<'
    }
    struct - exception {'>
     TermList tl = sl_make_slist();
     TermList tl2;
     Term *term, *res;

<'      foreach e [$t members] {'>
     tl2 = sl_make_slist();

     static Pred_Func_Rec *fr_<"[$e name]"> = NULL;
     if (! fr_<"[$e name]">)
	  fr_<"[$e name]"> = find_or_create_pred_func(declare_atom("<"[$e name]">"));

     term = decode_g3_<"[[$e type] mangle]">(<"[$e pass value data->[$e name]]">);
     add_to_tail(tl2, term);
     res = build_term_expr(build_expr_pfr_terms(fr_<"[$e name]">, tl2));
     add_to_tail(tl, res);
<'        } '>

     static Pred_Func_Rec *fr = NULL;
     if (! fr) fr = find_or_create_pred_func(declare_atom("<"[$t mangle]">"));

     return build_term_expr(build_expr_pfr_terms(fr, tl));

<'
    }
    union {'>
     TermList tl = sl_make_slist();
     TermList tl2;
     Term *term, *res;

<'     set e [$t discr] '>
     tl2 = sl_make_slist();

     static Pred_Func_Rec *fr_<"[$e name]"> = NULL;
     if (! fr_<"[$e name]">)
	  fr_<"[$e name]"> = find_or_create_pred_func(declare_atom("<"[$e name]">"));

     term = decode_g3_<"[$e mangle]">(<"[$e pass value data->_d]">);
     add_to_tail(tl2, term);
     res = build_term_expr(build_expr_pfr_terms(fr_<"[$e name]">, tl2));
     add_to_tail(tl, res);

     switch (data->_d) {
<'      foreach e [$t members] {'>
<'        foreach v [$e value] {'>
<'          if {$v != ""} {'>
     case <"[language::cname $v]">:
<'          } else {'>
     default:
<'          }'>
<'        }'>
	 {
	   tl2 = sl_make_slist();

	   static Pred_Func_Rec *fr_<"[$e name]"> = NULL;
	   if (! fr_<"[$e name]">)
	     fr_<"[$e name]"> = find_or_create_pred_func(declare_atom("<"[$e name]">"));

	   term = decode_g3_<"[[$e type] mangle]">(<"[$e pass value data->_u.[$e name]]">);
	   add_to_tail(tl2, term);
	   res = build_term_expr(build_expr_pfr_terms(fr_<"[$e name]">, tl2));
	   add_to_tail(tl, res);
	 }
<'        } '>
   }
     static Pred_Func_Rec *fr = NULL;
     if (! fr) fr = find_or_create_pred_func(declare_atom("<"[$t mangle]">"));

     return build_term_expr(build_expr_pfr_terms(fr, tl));

<'   }
    default { '>
      fprintf(stderr, "decode_g3_<"[$t mangle]"> has not been properly defined... report this to the genom3_openprs template maintainer (felix@laas.fr).\n");

      return build_nil();
<'      puts stderr "decode_g3_: unexpected type: [$t kind]" 
  }
}'>
}
<'}'>


Term *build_<"$comp">_exception_term(char *string, Term *detail)
{
     static Pred_Func_Rec *fr = NULL;
     if (!fr) fr= find_or_create_pred_func(declare_atom("exception"));

     TermList tl = sl_make_slist();

     add_to_tail(tl,PUMakeTermString(string));
     add_to_tail(tl,detail);
     return build_term_expr(build_expr_pfr_terms(fr, tl));
}

Term *genom_oprs_<"$comp">_exception(genom_event e, const void *detail)
{
     /* The beginning is really stolen from jason.c */
     char *s = NULL;

     if (e == genom_syserr_id) {
	  uint32_t e = ((genom_syserr_detail *)detail)->code;

	  s = strerror(e);
	  return build_<"$comp">_exception_term("SysErr", build_string(s));
	  
     } else if (e == genom_unkex_id) {

	  const char *s1 = genom_unkex_id;
	  char *s2 = ((genom_unkex_detail *)detail)->what;
	  s = malloc(strlen(s1) + strlen(s2) + 2);
       
	  sprintf(s, "%s %s", s1, s2);
	  Term * res = build_<"$comp">_exception_term(s, build_nil());
	  free(s);
	  return res;
     }
<'foreach e [$component types public] {'>
<'  if {[$e kind] != "exception"} continue'>
     else if (e == <"[$e cname]">_id) {
<'  if {[llength [$e members]]} {'>
       return build_<"$comp">_exception_term("<"[$e cname]">", decode_g3_<"[$e mangle]">(detail));
  <'  } else {'>
	return build_<"$comp">_exception_term("<"[$e cname]">", build_nil());
  <'  }'>
     }
<'}'>

  assert(!"unknown genom exception");
  return build_<"$comp">_exception_term("", build_nil());
}
