<'
# Copyright (c) 2012-2017 LAAS/CNRS
# All rights reserved.
#
# Redistribution and use  in source  and binary  forms,  with or without
# modification, are permitted provided that the following conditions are
# met:
#
#   1. Redistributions of  source  code must retain the  above copyright
#      notice and this list of conditions.
#   2. Redistributions in binary form must reproduce the above copyright
#      notice and  this list of  conditions in the  documentation and/or
#      other materials provided with the distribution.
#
#                                      Felix Ingrand on July 20 2012
#

if {[llength $argv] != 1} { error "expected arguments: component" }
lassign $argv component

set types [$component types public]

# compute handy shortcuts
set comp [$component name]
set COMP [string toupper [$component name]]

set opredicate [list]
set predicate [list]
set function [list]
set atom [list]
set ff [list]


lang c
'>

#include <stdlib.h>
#include <stdio.h>

#include <macro-pub.h>
#include <opaque-pub.h>
#include <constant-pub.h>
#include <oprs-type-pub.h>
#include <pu-mk-term_f.h>
#include <pu-parse-tl_f.h>

#include <oprs-type_f-pub.h>
#include <oprs-sprint_f-pub.h>
#include <action_f-pub.h>

#include "genom3/c/client.h"

#include <genom-oprs.h>
#include <genom-oprs_f.h>


#include "genom3/c/client.h"
#include "<"$comp">/c/client.h"
#include "<"$comp">EncodeDecodeOpenprs_f.h"

/*----------------------------------------------------------------------*/
/*
 * Requests declaration
 */
static void init_<"$comp">_service_table (Mod *mod)
{
<'foreach s [$component services] {'>
  declare_genom3_service("<"[$s name]">", mod,
			 genom_<"$comp">_client_<"[$s name]">_info,
			 (Encode_Func_Proto)encode_g3_<"$comp">_<"[$s name]">_input, 
			 (Decode_Func_Proto)decode_g3_<"$comp">_<"[$s name]">_output);
<'}'>
}


/*----------------------------------------------------------------------*/
/*
 *  Port declaration
 */
static void init_<"$comp">_port_table (Mod *mod)
{
<'foreach p [$component ports out] {'>
  declare_genom3_port("<"[$p name]">", mod,
		     genom_<"$comp">_client_<"[$p name]">_info,
		     (Decode_Func_Proto)decode_g3_<"$comp">_<"[$p name]">_port);
<'}'>
}

/*
 * This is the main function which declares everything.
 */
PBoolean init_<"$comp">_named_module(char *name)
{
     Mod *mod;

     if (genom_<"$comp">_client_info.protocol !=  genom_client_protocol) {
	  fprintf(stderr,"init_<"$comp">_named_module: protocol mismatch %d != %d.\n",
		  genom_<"$comp">_client_info.protocol, genom_client_protocol);
	  return FALSE;
     }

     mod = declare_genom3_module(name,  &genom_<"$comp">_client_info, genom_oprs_<"$comp">_exception);
     
     init_<"$comp">_service_table(mod);
     init_<"$comp">_port_table(mod);

     return TRUE;
}


Term * make_new_<"$comp">_module(TermList terms)
{
     char *mod_name;

     if (! (PUGetOprsParameters(terms, 1, ATOM, &mod_name))) {
	  fprintf(stderr,"Expecting an ATOM in make_new_<"$comp">_module.\n");
	  return build_nil();
     } else {
	  if (init_<"$comp">_named_module(mod_name))
	       return build_t();
	  else
	       return build_nil();
     }
}

void init_<"$comp">_module ()
{
     if (init_<"$comp">_named_module("<"$comp">")) /* This will create the first instance. */
	  make_and_declare_action("MAKE-NEW-<"$COMP">-MODULE", make_new_<"$comp">_module, 1);
}

